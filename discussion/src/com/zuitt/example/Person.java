package com.zuitt.example;

//interfaces implentation
//multiple implementations using interfaces
public class Person implements Actions, Greetings{
    public void sleep(){
        System.out.println("Zzzz...");
    }
    public void run(){
        System.out.println("Running");
    }
    public void morningGreet(){
        System.out.println("Good morning");
    }
    public void holidayGreet(){
        System.out.println("Happy Holidays!");
    }
}
