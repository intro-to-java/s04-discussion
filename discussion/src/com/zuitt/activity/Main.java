package com.zuitt.activity;

public class Main {
    public static void main(String[] args) {
        Phonebook pb = new Phonebook();
        Contact c = new Contact();
        c.setName("John Doe");
        c.setContactNumber("+639152468596");
        c.setAddress("Quezon City");
        Contact c2 = new Contact("Jane Doe", "+639162148573", "Caloocan City");

        pb.setContacts(c);
        pb.setContacts(c2);

        if(pb.getContacts().isEmpty()) {
            System.out.println("Phonebook is empty");
        } else {
//            for(int i = 0; i < pb.getContacts().size(); i++) {
//                System.out.println("-------------------");
//                System.out.println(pb.getContacts().get(i).getName());
//                System.out.println("-------------------");
//                System.out.println(pb.getContacts().get(i).getName() + " has the following registered number: ");
//                System.out.println(pb.getContacts().get(i).getContactNumber());
//                System.out.println(pb.getContacts().get(i).getName() + " has the following registered address: ");
//                System.out.println("my home in " + pb.getContacts().get(i).getAddress());
//                }

            for(Contact contact : pb.getContacts()) {
                System.out.println("-------------------");
                System.out.println(contact.getName());
                System.out.println("-------------------");
                System.out.println(contact.getName() + " has the following registered number: ");
                System.out.println(contact.getContactNumber());
                System.out.println(contact.getName() + " has the following registered address: ");
                System.out.println("my home in " + contact.getAddress());
            }


        }

    }
}
